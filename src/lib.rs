use std::{
    collections::{BTreeMap, HashMap},
    path::Path,
    sync::{Arc, RwLock},
    time::Instant,
};

use eyre::Result;
use log::{debug, info};
use osmpbf::{Element, ElementReader};
use tinyvec::{tiny_vec, TinyVec};

macro_rules! oneof {
    ( $x:expr, $( $y:expr ),* ) => {
        false $( || $x == $y )*
    };
}

type NodeToWaysMap = BTreeMap<i64, TinyVec<[i64; 1]>>;
type WayNodeLatLonSumMap = BTreeMap<i64, (f64, f64)>;

fn is_interesting_way(tags: &HashMap<&str, &str>) -> bool {
    if !tags.contains_key("building") {
        return false;
    }

    if let Some(value) = tags.get("amenity") {
        !oneof!(
            *value,
            "parking",
            "parking_space",
            "bench",
            "waste_basket",
            "bicycle_parking"
        )
    } else {
        tags.get("shop").is_some()
    }
}

fn is_interesting_node<'a>(mut tags: impl Iterator<Item = (&'a str, &'a str)>) -> bool {
    tags.any(|(key, value)| {
        (key == "amenity"
            && !oneof!(
                value,
                "parking",
                "parking_space",
                "bench",
                "waste_basket",
                "bicycle_parking"
            ))
            || key == "shop"
    })
}

pub struct InsertRow<'a> {
    pub r#type: &'a str,
    pub id: i64,
    pub lon: f64,
    pub lat: f64,
    pub tags: HashMap<&'a str, &'a str>,
    pub class: &'a str,
    pub subclass: &'a str,
}

pub struct Importer<P, F>
where
    P: AsRef<Path>,
    F: Fn(InsertRow) + Sync,
{
    osm_pbf_path: P,
    inserter: F,
}

impl<P, F> Importer<P, F>
where
    P: AsRef<Path>,
    F: Fn(InsertRow) + Sync,
{
    pub fn new(osm_pbf_path: P, inserter: F) -> Importer<P, F> {
        Self {
            osm_pbf_path,
            inserter,
        }
    }

    pub fn import(&mut self) -> Result<()> {
        info!("Starting the import");

        let way_node_lat_lon_sum_map = {
            let node_to_ways_map = self.read_ways()?;
            self.read_nodes_and_store_interesting(&node_to_ways_map)?
        };
        self.store_ways(&way_node_lat_lon_sum_map)?;

        Ok(())
    }

    pub fn read_ways(&mut self) -> Result<Arc<RwLock<NodeToWaysMap>>> {
        info!("Reading ways");
        let start_instant = Instant::now();

        let reader = ElementReader::from_path(&self.osm_pbf_path)?;

        let node_to_ways_map: NodeToWaysMap = BTreeMap::new();
        let node_to_ways_map = Arc::new(RwLock::new(node_to_ways_map));

        reader
            .par_map_reduce(
                |element| {
                    if let Element::Way(way) = element {
                        if is_interesting_way(&way.tags().collect::<HashMap<_, _>>()) {
                            // skipping first node as it is assumed that shops are closed ways only
                            let Some(first_node_id) = way.refs().next() else {
                                return;
                            };
                            let Some(last_node_id) = way.refs().last() else {
                                return;
                            };
                            if first_node_id != last_node_id {
                                return;
                            }

                            let mut node_to_ways_map = node_to_ways_map.write().unwrap();
                            for node_id in way.refs().skip(1) {
                                node_to_ways_map
                                    .entry(node_id)
                                    .and_modify(|entry| entry.push(way.id()))
                                    .or_insert(tiny_vec![way.id()]);
                            }
                        }
                    }
                },
                || (),
                |(), ()| (),
            )
            .unwrap();

        debug!("Elapsed: {:.2?}", start_instant.elapsed());

        Ok(node_to_ways_map)
    }

    pub fn read_nodes_and_store_interesting(
        &mut self,
        node_to_ways_map: &Arc<RwLock<NodeToWaysMap>>,
    ) -> Result<Arc<RwLock<WayNodeLatLonSumMap>>> {
        info!("Reading nodes and storing interesting");
        let start_instant = Instant::now();

        let reader = ElementReader::from_path(&self.osm_pbf_path)?;

        let way_node_lat_lon_sum_map: BTreeMap<i64, (f64, f64)> = BTreeMap::new();
        let way_node_lat_lon_sum_map = Arc::new(RwLock::new(way_node_lat_lon_sum_map));

        reader
            .par_map_reduce(
                |element| {
                    match element {
                        Element::Node(_) => {
                            todo!();
                        }
                        Element::DenseNode(dense_node) => {
                            if is_interesting_node(dense_node.tags()) {
                                let (class, subclass) = if let Some((_, value)) =
                                    dense_node.tags().find(|(key, _)| key == &"amenity")
                                {
                                    ("amenity", value)
                                } else if let Some((_, value)) =
                                    dense_node.tags().find(|(key, _)| key == &"shop")
                                {
                                    ("shop", value)
                                } else {
                                    return;
                                };

                                let tag_map = dense_node.tags().collect::<HashMap<&str, &str>>();

                                (self.inserter)(InsertRow {
                                    r#type: "N",
                                    id: dense_node.id,
                                    lon: dense_node.lon(),
                                    lat: dense_node.lat(),
                                    tags: tag_map,
                                    class,
                                    subclass,
                                });
                            }

                            {
                                let node_to_ways_map = node_to_ways_map.read().unwrap();
                                if let Some(way_ids) = node_to_ways_map.get(&dense_node.id) {
                                    let mut way_node_lat_lon_sum_map =
                                        way_node_lat_lon_sum_map.write().unwrap();
                                    for way_id in way_ids {
                                        way_node_lat_lon_sum_map
                                            .entry(*way_id)
                                            .and_modify(|entry| {
                                                entry.0 += dense_node.lat();
                                                entry.1 += dense_node.lon();
                                            })
                                            .or_insert((dense_node.lat(), dense_node.lon()));
                                    }
                                };
                            }
                        }
                        Element::Way(_) | Element::Relation(_) => (),
                    };
                },
                || (),
                |(), ()| (),
            )
            .unwrap();

        debug!("Elapsed: {:.2?}", start_instant.elapsed());

        Ok(way_node_lat_lon_sum_map)
    }

    pub fn store_ways(
        &mut self,
        way_node_lat_lon_sum_map: &Arc<RwLock<WayNodeLatLonSumMap>>,
    ) -> Result<()> {
        info!("Storing ways");
        let start_instant = Instant::now();

        let reader = ElementReader::from_path(&self.osm_pbf_path)?;

        reader
            .par_map_reduce(
                |element| {
                    if let Element::Way(way) = element {
                        let way_node_lat_lon_sum_map = way_node_lat_lon_sum_map.read().unwrap();
                        if let Some(lat_lon_sum) = way_node_lat_lon_sum_map.get(&way.id()) {
                            let (class, subclass) = if let Some((_, value)) =
                                way.tags().find(|(key, _)| key == &"amenity")
                            {
                                ("amenity", value)
                            } else if let Some((_, value)) =
                                way.tags().find(|(key, _)| key == &"shop")
                            {
                                ("shop", value)
                            } else {
                                return;
                            };

                            // skipping first node as it is assumed that shops are closed ways only
                            let node_count = way.refs().count() - 1;
                            #[allow(clippy::cast_precision_loss)]
                            let lat = lat_lon_sum.0 / node_count as f64;
                            #[allow(clippy::cast_precision_loss)]
                            let lon = lat_lon_sum.1 / node_count as f64;

                            let tag_map = way.tags().collect::<HashMap<&str, &str>>();

                            (self.inserter)(InsertRow {
                                r#type: "W",
                                id: way.id(),
                                lon,
                                lat,
                                tags: tag_map,
                                class,
                                subclass,
                            });
                        }
                    }
                },
                || (),
                |(), ()| (),
            )
            .unwrap();

        debug!("Elapsed: {:.2?}", start_instant.elapsed());
        Ok(())
    }
}
