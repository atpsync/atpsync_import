use std::path::PathBuf;

use clap::{command, Parser};

/// Program to import .osm.pbf file into `PostGIS` database
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
pub struct Args {
    /// Path to the source *.osm.pbf file
    pub source_path: PathBuf,

    /// `PostgreSQL`/`PostGIS`` conninfo
    pub conninfo: String,
}
