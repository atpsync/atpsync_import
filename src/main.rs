pub mod cli;

use std::sync::{Arc, Mutex};

use atpsync_import::{Importer, InsertRow};
use clap::Parser;
use cli::Args;
use eyre::{eyre, Result};
use log::info;
use postgres::types::Json;

const TABLE_NAME: &str = "pois";

fn main() -> Result<()> {
    env_logger::Builder::new()
        .filter(None, log::LevelFilter::Info)
        .filter(Some("atpsync_import"), log::LevelFilter::Debug)
        .init();
    let args = Args::parse();

    let mut db = postgres::Client::connect(&args.conninfo, postgres::NoTls)?;

    let mut transaction = db.transaction().unwrap();
    info!("Deleting POIs");
    transaction.execute(&format!("DELETE FROM {}", "pois"), &[])?;

    let transaction = Arc::new(Mutex::new(transaction));

    {
        let transaction = transaction.clone();
        let inserter = move |row: InsertRow| {
            let tags_brand = row.tags.get("brand").copied();
            let tags_brand_wikidata = row.tags.get("brand:wikidata").copied();
            let tags_name = row.tags.get("name").copied();
            let tags_operator = row.tags.get("operator").copied();
            let tags_ref = row.tags.get("ref").copied();

            let mut transaction = transaction.lock().unwrap();
            transaction.execute(
                &format!("INSERT INTO {TABLE_NAME} (type, id, geom, tags, tags_brand, tags_brand_wikidata, tags_name, tags_operator, tags_ref, class, subclass) VALUES ($1, $2, ST_Point($3, $4, 4326), $5, $6, $7, $8, $9, $10, $11, $12)"),
                &[
                    &row.r#type,
                    &row.id,
                    &row.lon,
                    &row.lat,
                    &Json(&row.tags),
                    &tags_brand,
                    &tags_brand_wikidata,
                    &tags_name,
                    &tags_operator,
                    &tags_ref,
                    &row.class,
                    &row.subclass
                ],
            ).unwrap();
        };
        let mut importer = Importer::new(args.source_path, inserter);
        importer.import()?;
    }

    {
        let transaction = Arc::try_unwrap(transaction)
            .map_err(|_| eyre!("unwrapping arc failed"))?
            .into_inner()
            .map_err(|_| eyre!("unwrapping mutex failed"))?;
        transaction.commit().unwrap();
    }

    Ok(())
}
