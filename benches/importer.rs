use std::{fs, os::unix::process::CommandExt, path::PathBuf, process, time::Duration};

use criterion::{criterion_group, criterion_main, Criterion};
use directories::ProjectDirs;

use atpsync_import::Importer;

fn ensure_test_file() -> PathBuf {
    let project_dirs = ProjectDirs::from("cz", "vfosnar", "atpsync_import").unwrap();
    let test_file_path = project_dirs.cache_dir().join("czech-republic.osm.pbf");
    if !test_file_path.exists() {
        fs::create_dir_all(test_file_path.parent().unwrap()).unwrap();

        let download_path = test_file_path.with_file_name(test_file_path.file_name().unwrap());
        fs::create_dir_all(download_path.parent().unwrap()).unwrap();

        process::Command::new("curl")
            .args([
                "-L",
                "-o",
                download_path.to_str().unwrap(),
                "https://download.geofabrik.de/europe/czech-republic-latest.osm.pbf",
            ])
            .exec();

        fs::rename(&download_path, &test_file_path).expect("Caching failed");
    }
    test_file_path
}

fn bench(c: &mut Criterion) {
    let test_file_path = ensure_test_file();
    let test_file_path = test_file_path.as_path();

    let mut importer = Importer::new(test_file_path, |_| {});

    c.bench_function("read_ways", |b| {
        b.iter(|| {
            importer.read_ways().unwrap();
        });
    });

    {
        let node_to_ways_map = importer.read_ways().unwrap();
        c.bench_function("read_nodes_and_store_interesting", |b| {
            b.iter(|| {
                importer
                    .read_nodes_and_store_interesting(&node_to_ways_map.clone())
                    .unwrap();
            });
        });
    }

    {
        let node_to_ways_map = importer.read_ways().unwrap();
        let way_node_lat_lon_sum_map = importer
            .read_nodes_and_store_interesting(&node_to_ways_map)
            .unwrap();
        c.bench_function("store_ways", |b| {
            b.iter(|| {
                importer
                    .store_ways(&way_node_lat_lon_sum_map.clone())
                    .unwrap();
            });
        });
    }
}

criterion_group!(
    name = benches;
    config = Criterion::default().sample_size(10).measurement_time(Duration::from_secs(90));
    targets = bench
);
criterion_main!(benches);
