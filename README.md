# atpsync-import

Tool to extract and insert POI data for atpsync into PostGIS efficiently.

- `osmpbfreader` is ~3x slower than `osmpbf`
- can't calculate diff based on versions as element doesn't change version if one of its nodes changes location
- insert performance can probably be improved by bulk inserting data
